package com.example.androidex1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import org.w3c.dom.Text


class GetStarted : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_get_started)

        supportActionBar?.hide()
    }
}